package main

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"guitou.cm/msvc/auth/controllers"
)

type AppHandler func(http.ResponseWriter, *http.Request) *controllers.AppError

func (fn AppHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := fn(w, r); err != nil {
		http.Error(w, err.Message, err.StatusCode)
	}
}

func NewRouter() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode("I'm alive")
	})

	r.Handle("/signin", AppHandler(controllers.SignIn)).Methods(http.MethodPost)
	r.Handle("/signup", AppHandler(controllers.SignUp)).Methods(http.MethodPost)
	r.HandleFunc("/signout", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
	}).Methods(http.MethodPost)
	r.Handle("/check", AppHandler(controllers.Check)).Methods(http.MethodPost)

	r.Handle("/password/forgot", AppHandler(controllers.ForgetPassword)).Methods(http.MethodPost)
	r.Handle("/password/forgot/verify", AppHandler(controllers.VerifyResetPasswordToken)).Methods(http.MethodPost)
	r.Handle("/password/reset", AppHandler(controllers.ResettingPassword)).Methods(http.MethodPost)

	// r.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	// 	// this handler wasn't working as expected
	// 	// w.WriteHeader(501)
	// 	// w.Write([]byte(`{"status":501,"message":"501: Not implemented."}`))
	// 	w.Write([]byte(`{ message: 'Route Not Found' }`))
	// })

	r.NotFoundHandler = r.NewRoute().BuildOnly().HandlerFunc(http.NotFound).GetHandler()

	return r
}
