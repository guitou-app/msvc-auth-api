package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/handlers"

	"github.com/rs/cors"
	"guitou.cm/msvc/auth/db"
	"guitou.cm/msvc/auth/rabbitmq"
)

func loggingMiddleware(next http.Handler) http.Handler {
	log.Printf("loging middleware")

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		start := time.Now()

		log.Printf(
			"%s\t%s\t%s\t",
			r.Method,
			r.RequestURI,
			time.Since(start),
		)
		next.ServeHTTP(w, r)
	})
}

func init() {
	envVariables := []string{
		"RABBITMQ_URI",
		"RABBITMQ_EXCHANGES",
		"MONGODB_URI",
		"MONGODB_DBNAME",
		"RABBITMQ_AUTH_PASSWORD_FORGET_EXCHANGE",
		"RABBITMQ_AUTH_PASSWORD_RESET_EXCHANGE",
		"RABBITMQ_AUTH_SIGN_UP_EXCHANGE",
		"RABBITMQ_AUTH_SIGN_IN_EXCHANGE",
	}
	for _, env := range envVariables {
		_, present := os.LookupEnv(env)
		if !present {
			log.Fatalf("%s env variable is not defined.", env)
		}
	}
}

func main() {
	db.OpenDB()
	defer db.CloseDB()

	rabbitmq.Connect()
	defer rabbitmq.Close()

	r := NewRouter()
	r.Use(loggingMiddleware)

	loggedRouter := handlers.LoggingHandler(os.Stdout, r)
	handler := cors.AllowAll().Handler(loggedRouter)

	log.Println("Listen on port :6000")
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), handler))
}
